﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DirectoryInformation
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryInfo directoriya = new DirectoryInfo("D:\\");
            DirectoryInfo[] poddirectoriya = directoriya.GetDirectories();

            foreach (DirectoryInfo dd in poddirectoriya)
            {
                if (dd.Attributes == FileAttributes.Directory)
                {
                    FileInfo[] spisokfile = dd.GetFiles();
                    foreach (FileInfo fi in spisokfile)
                    {
                        Console.WriteLine(fi.ToString());
                    }
                }
            }
            Console.ReadKey();
        }
    }
}
