﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace RunProgramm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSystemInfo_Click(object sender, EventArgs e)
        {
            try
            {
                Process SysInfo = new Process();
                SysInfo.StartInfo.ErrorDialog = true;
                SysInfo.StartInfo.FileName = "C:\\Program Files\\Common Files\\Microsoft Shared\\MSInfo\\msinfo32.exe";
                SysInfo.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnRegEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Process RegEdit = new Process();
                RegEdit.StartInfo.ErrorDialog = true;
                RegEdit.StartInfo.FileName = "C:\\Windows\\regedit.exe";
                RegEdit.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnExplorer_Click(object sender, EventArgs e)
        {
            try
            {
                Process Explorer = new Process();
                Explorer.StartInfo.ErrorDialog = true;
                Explorer.StartInfo.FileName = "C:\\Windows\\explorer.exe";
                Explorer.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
