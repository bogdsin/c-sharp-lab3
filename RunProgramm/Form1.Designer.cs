﻿namespace RunProgramm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSystemInfo = new System.Windows.Forms.Button();
            this.btnRegEdit = new System.Windows.Forms.Button();
            this.btnExplorer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSystemInfo
            // 
            this.btnSystemInfo.Location = new System.Drawing.Point(63, 83);
            this.btnSystemInfo.Name = "btnSystemInfo";
            this.btnSystemInfo.Size = new System.Drawing.Size(168, 23);
            this.btnSystemInfo.TabIndex = 0;
            this.btnSystemInfo.Text = "Информация о системе";
            this.btnSystemInfo.UseVisualStyleBackColor = true;
            this.btnSystemInfo.Click += new System.EventHandler(this.btnSystemInfo_Click);
            // 
            // btnRegEdit
            // 
            this.btnRegEdit.Location = new System.Drawing.Point(63, 112);
            this.btnRegEdit.Name = "btnRegEdit";
            this.btnRegEdit.Size = new System.Drawing.Size(168, 23);
            this.btnRegEdit.TabIndex = 1;
            this.btnRegEdit.Text = "Редактор реестра";
            this.btnRegEdit.UseVisualStyleBackColor = true;
            this.btnRegEdit.Click += new System.EventHandler(this.btnRegEdit_Click);
            // 
            // btnExplorer
            // 
            this.btnExplorer.Location = new System.Drawing.Point(63, 141);
            this.btnExplorer.Name = "btnExplorer";
            this.btnExplorer.Size = new System.Drawing.Size(168, 23);
            this.btnExplorer.TabIndex = 2;
            this.btnExplorer.Text = "Проводник";
            this.btnExplorer.UseVisualStyleBackColor = true;
            this.btnExplorer.Click += new System.EventHandler(this.btnExplorer_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnExplorer);
            this.Controls.Add(this.btnRegEdit);
            this.Controls.Add(this.btnSystemInfo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSystemInfo;
        private System.Windows.Forms.Button btnRegEdit;
        private System.Windows.Forms.Button btnExplorer;
    }
}

