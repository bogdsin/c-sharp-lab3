﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileExists
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Название файла: ");
            string nazvaniefayla = Console.ReadLine();
            if (File.Exists(nazvaniefayla))
            {
                StreamReader chitat = new StreamReader(nazvaniefayla);
                string A;
                A = chitat.ReadToEnd();
                chitat.Close();
                Console.WriteLine(A);
            }
            else
            {
                Console.WriteLine("Файл с указанным именем не существует. Сoздать его Y/N");
                string rezultatvibora = Console.ReadLine();
                if ((rezultatvibora == "Y") || (rezultatvibora == "y"))
                {
                    StreamWriter pisat = new StreamWriter(nazvaniefayla);
                    Console.WriteLine("Введите текст");
                    string vvedemtext = Console.ReadLine();
                    pisat.WriteLine(vvedemtext);
                    pisat.Close();
                    Console.WriteLine("Файл создан");
                }
                else
                {
                    Console.WriteLine("Ничего не будет созданно");
                }
            }
            Console.ReadKey();
        }
    }
}
