﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TextEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void mnuOpen_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            string fileName = openFileDialog1.FileName;
            FileStream filestream = File.Open(fileName, FileMode.Open, FileAccess.Read);
            if (filestream != null)
            {
                StreamReader streamreader = new StreamReader(filestream);
                txtBox.Text = streamreader.ReadToEnd();
                filestream.Close();
            }
        }

        private void mnuSave_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
            string fileName = saveFileDialog1.FileName;
            FileStream filestream = File.Open(fileName, FileMode.Create, FileAccess.Write);
            if (filestream != null)
            {
                StreamWriter streamwriter = new StreamWriter(filestream);
                streamwriter.Write(txtBox.Text);
                streamwriter.Flush();
                filestream.Close();
            }
        }
    }
}
