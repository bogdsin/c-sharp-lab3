﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileStream
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader Chitat = new StreamReader("../../1.txt");
            string A;
            A = Chitat.ReadToEnd();
            Chitat.Close();
            Console.WriteLine(A);

            StreamWriter Pisat = new StreamWriter("../../1.txt", true, System.Text.Encoding.Unicode);
            Pisat.Write("Новый текст");
            Pisat.Close();

            StreamReader ChitatSnova = new StreamReader("../../1.txt");
            string B;
            B = ChitatSnova.ReadToEnd();
            ChitatSnova.Close();
            Console.WriteLine(B);

            Console.ReadKey();
        }
    }
}
