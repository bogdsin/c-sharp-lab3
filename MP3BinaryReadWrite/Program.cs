﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MP3BinaryReadWrite
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                FileStream Chtenie = new FileStream("music.mp3", FileMode.Open);

                BinaryReader br = new BinaryReader(Chtenie);

                byte[] n = br.ReadBytes(1000000);
                br.Close();
                FileStream Zapis = new FileStream("newmusic.mp3", FileMode.CreateNew, FileAccess.Write);
                BinaryWriter wr = new BinaryWriter(Zapis);
                wr.Write(n);
                br.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
